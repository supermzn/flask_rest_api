import random
import time
import threading

from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/random/")
def getRandomNumber():
    return str(random.randint(1,101))

def sleepyWorker(delay):
    time.sleep( delay )

@app.route("/random/<int:delay>")
def getRandomNumberWithDelay(delay):
    if delay < 0 or delay > 5:
        delay = random.uniform(0.1, 5.0)
    t = threading.Thread(target=sleepyWorker, args=(delay,))
    t.start()
    t.join()
    return str(random.randint(1,101))


if __name__ == '__main__':
    app.run(debug=True),
